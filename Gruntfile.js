'use strict';

module.exports = function (grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    watch: {
      iconfont: {
        files: ['svg/*'],
        tasks: ['webfont']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          'fonts/*.html',
        ]
      }
    },

    connect: {
      options: {
        port: 9200,
        open: true,
        livereload: 36729,
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              connect.static('fonts')
            ];
          }
        }
      },
    },

    webfont: {
      icons: {
        src: 'svg/*.svg',
        dest: 'fonts',
        options: {
          font: "iconfont",
          order: 'svg,eot,woff,ttf',
          hashes: false,
          stylesheet: 'less',
          engine: 'node',
          syntax: 'bootstrap',
          relativeFontPath: '/fonts/',
          templateOptions: {
            baseClass: '',
            classPrefix: 'iconfont-',
          }
        }
      }
    },
  });


  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'webfont',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('default', [
    'webfont',
  ]);
};
